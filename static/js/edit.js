//保存代码
function Code(isChangeBanben){   
    let code = codeMain.getValue()
    let model = $("#debugModel option:selected").val()
    let path = $(".fileDir").html()
    let Data = {};

    //不是切换版本，才保存版本
    if(!isChangeBanben){
        //保存版本
        if(!editHistoryList[path]){
            editHistoryList[path] = []
        }
        
        let banben = {}
        banben.time = new Date().toLocaleString( )
        banben.info = code
        editHistoryList[path].push(banben)
        if(editHistoryList[path].length > 5){
            editHistoryList[path].splice(0,(editHistoryList[path].length - 5))
        }
        //保存到缓存中去
        window.localStorage.setItem("editHistoryList",JSON.stringify(editHistoryList))

        let options = '<option data-path=""  data-i="">请选择历史版本</option>'
        if(editHistoryList[path]){
            //进行历史版本渲染
            for(let i = 0;i <editHistoryList[path] .length; i++){
                options += '<option data-path="'+path+'"  data-i="'+i+'">'+editHistoryList[path][i].time+'</option>'
            }
        }
        $("#editHistoryList").html(options)        
    }
    setTimeout(()=>$(".selected .isEditing").removeClass("isEditingActive"),500)


    Data.type = "CODE";
    Data.path = path;
    Data.data = code;
    ws.send(json(Data)) 
    if($.inArray(model,fileSystemModels) != -1){
        DeBug()
    }
}

function editCode(){
    $(".selected .isEditing").addClass("isEditingActive")
}
//版本修改
function changeBanben(obj){
    let path = $("#editHistoryList option:selected").attr('data-path')
    let index = $("#editHistoryList option:selected").attr('data-i')
    if(path.length > 0 && index.length > 0){
        let html = editHistoryList[path][index].info
        codeMain.setValue(html,-1)
        Code(1)
        layer.msg("版本切换成功！")
    }
}

//修改字体大小
function SetFontSize(obj){
    $(".codeBox .codeMain").css({
        fontSize:$(obj).val() + "px"
    })
}
function SetLang(){
    let lan = $("#SetLang").find("option:selected").text();
    Lang(codeMain,lan + "." + lan)
}
//修改编程语言
function Lang(obj,path){
    let  res  = path.split(".")
    let  name = res[res.length - 1]
    let  CReg = 0 //验证是否识别到
    for(let i = 0; i < Langs.length; i++){
        if(name == Langs[i][0]){
            obj.getSession().setMode("ace/mode/"+ Langs[i][1]);
            //修改编程语言修改项
            $("#SetLang").find("option").eq(i).prop("selected",true)
            CReg++
            break
        }
    }
    if(CReg == 0){
        obj.getSession().setMode("ace/mode/html"); //默认没匹配初始化为html
        $("#SetLang").find("option").eq(0).prop("selected",true)
    }
    $(".codeBox").show("slow")
}