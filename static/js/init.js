//编程语言
let Langs = [
    ["文本","text"],
    ["php","php"],
    ["css","css"],
    ["django","django"],
    ["go","golang"],
    ["js","javascript"],
    ["ts","typescript"],
    ["objectivec","objectivec"],
    ["php_laravel_blade","php_laravel_blade"],
    ["md","markdown"],
    ["markdown","markdown"],
    ["apache_conf","apache_conf"],
    ["powershell","powershell"],
    ["vbscript","vbscript"],
    ["ruby","ruby"],
    ["sass","sass"],
    ["sql","sql"],
    ["swift","swift"],
    ["html","html"],
    ["htm","html"],
    ["tpl","html"],
    ["json","json"],
    ["java","java"],
    ["jsp","jsp"],
    ["lua","lua"],
    ["py","python"],
    ["xml","xml"],
    ["wxss","css"],
    ["wxml","html"],
    ["sh","sh"],
    ["nginx","nginx"],
]
let isShowPre    = false
let databases = [];
let apiBodyBox = null;
let openEditCounts = 0;
let ApiResponsModel = 'json';
let setIntervalId = 0;
let x = 0;
let codeMain = null;
let XId = 0;
let reg = /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([\w-~]+).)+([\w-~\/])+/
let ipReg = /[1-9]+(\.\d+)+/  //ipv4 ipv6 
let urlModels  = ["NORMAL","GET","POST","YALI-GET","YALI-POST"]
let modelsUrlValues = {}  //模式输入框寄存器
let apiModels = ["GET","POST"]
let yaliModels = ["YALI-GET","YALI-POST"]
let rightModels = ["YALI-GET","YALI-POST","REGXP","CHEST"]
let fileSystemModels = ["NORMAL","GET","POST"]
let fileSystemModels2 = ["STRSEARCH","DIRSEARCH"]
let CurrentDir = ""  //路径寄存器
let PageWidth = 0
let PageHeight = 0
let fileOpenLists = [] //{path:'c:/',line:0}
let ChangeChanelCount = 0  //和openmysql  closemysql配置使用的 监听是否初始化过
let editHistoryList = {} //历史版本寄存器
let dragLists = [
    //{element:"#Mysqlbox",scope:"body"},
    {element:".performance",scope:"body"},
    {element:".QL",scope:"body"},
    {element:".setHetBox",scope:"body",allowBox:'.serachP'},
    //{element:".AnalysisSqlResult .tableBox",scope:"body"},
    {element:".showProfileBox",scope:"body"},
    {element:".explainSqlBox",scope:"body"},
    {element:".showSelectResBox",scope:"body"},
    {element:".showTbaleDescBox",scope:"body"},
    //{element:".setHetBox",scope:"body",disable:true, disableInput:"#iframeWidthSet,#iframeHeightSet"},
    // {handle:".serachP",scope:".setHetBox"},
] 

let openLine = 1
//

//表情
let jpgCount = 11
let gifCount = 3
let netImgHistory = []

//拖曳列表
let disableList = [
    ".serachP",
    ".performance",
    ".pQL",
]
//性能耗时对象
let perFormance = {
    isOpen:false,
    NORMAL:{},
    GET:{},
    POST:{},
    YALIGET:{},
    YALIPOST:{}
}


//closePerformance
let showPerformance = true
let hasPerformance  = false

//调试的开始时间
let startTime = 0
//调试的结束时间
let endTime = 0

let layerIndex  = false
let hsitoryHasShow = false
//let msgLists = [] //废弃个变量
//let lastMsgTime =  0  //废弃个变量
let User    = {
         name : '',
         header:'',
         sign: '',
         QL:[],
         SL:[],
}



let QLList = []
let DefaultQlObj = {
    type:"QL",
    Sign:"QL",
    Name:"技术学习交流区",
    Msg:[],
    NoReadMsg:[],
    Header:"/static/header/QL.jpg"
}
let ContactIng =    DefaultQlObj //这个是存放的Sign
let contactList = [
    DefaultQlObj
]
let headers = ''
for (let i =1; i <= 30; i++){
    headers += '<img src="/static/header/'+i+'.jpg" class="headerImg" onclick="setHeader(this)"/>'
}
let UserNameForm     = `
        <div class="UserNameBox">
        <p>
        网络图片:<input type="text" id="addNetImgUrl" placeholder="请输入您喜欢的网络表情图片" />
         <button onclick="addNetImg()" class="addNetImgUrlButton">确定</button> 
        </p><hr/>
          <p class="headerList">${headers}</p>
          <p><input type="text" id="UserName" placeholder="请输入您喜欢的昵称"/> <button onclick="setUserName()">提交</button></p>
        </div>                        
`
let AnalysisSqlHtml = `
      <ul class="AnalysisSqlBoxUl">
      <li class="currentSqlBox"></li>
      <li class="AnalysisSqlBoxLi">
        <div class="AnalysisSqlBox">
            <p class="AnalysisSqlDatabase">MysqlDatasSelect</p>
            <p class="AnalysisSqlTextarea">
               需分析的语句： <input type="text" class="Sql" placeholder="仅支持单条语句，结尾请不要带分号！" />
            </p>
            <p><span class="red">你也可以输入你自己需要分析的语句【注意：仅支持单条语句，回车可执行！由于表名是通过正则匹配，部分复杂语句部分表名可能会匹配有误】</span></p>

            <button onclick="AnalysisSql()" class="AnalysisSql">开始语句分析</button>&nbsp;
            <button onclick="OpenAnalysisSqlLeft(0)" class="AnalysisSql" style="background:#000">隐藏左边</button>&nbsp;
            <button onclick="OpenAnalysisSqlLeft(1)" class="AnalysisSql"  style="background:red">打开左边</button>&nbsp;&nbsp;
            <br/>
            <br/>
            <ul class="AnalysisSqlResult"></ul>
        </div>
        </li>
 </ul>
 <div style="clear:both;"></div>
`
