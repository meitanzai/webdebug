<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{.title}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Cache" content="no-cache">
    <link rel="shortcut icon" type="image/x-icon" href="/static/img/favicon.ico?t={{.time}}" />
    <link rel="stylesheet" href="/static/css/main.css?t={{.time}}">
    <link rel="stylesheet" href="/static/layui/css/layui.css?t={{.time}}">
    <script src="/static/layui/layui.all.js?t={{.time}}"></script>
    <script src="/static/js/jquery-1.10.2.js?t={{.time}}"></script>
    <script src="/static/js/init.js?t={{.time}}"></script>
    <script src="/static/ace/ace.js?t={{.time}}"></script>
    <script src="/static/ace/ext-beautify.js?t={{.time}}"></script>
    <script src="/static/ace/ext-language_tools.js?t={{.time}}"></script>
    <script src="/static/ace/theme-monokai.js?t={{.time}}"></script>
    <script src="/static/js/Tdrag.js?t={{.time}}"></script>
    <script src="/static/js/echarts.min.js?t={{.time}}"></script>
    <script src="/static/js/main.js?t={{.time}}"></script>
    <script src="/static/js/websocket.js?t={{.time}}"></script>
    <script src="/static/js/im.js?t={{.time}}"></script>
    <script src="/static/js/com.js?t={{.time}}"></script>
    <script src="/static/js/edit.js?t={{.time}}"></script>
</head>
<body>
<iframe src="/static/instructions.html?t=12" frameborder="0"  id="iframe"></iframe>
<iframe src="" frameborder="0"  id="show" style="display: none;"></iframe>
<div class="apiBody" id="apiBody">
    <pre class="apiBodyPre" id="apiBodyPre"></pre>
    <div class="apiBodyBox" id="apiBodyBox" style="height: 100%;"></div>
</div>
<div class="performance">
    <div class="performance-haoshi"></div>
    <div class="performance-map" id="performance-map"></div>
    <span class="close" onclick="closePerformance(this)"></span>
</div> 
<div class="setHetBox">
    <p class="iframeWidthSetP" style="position: relative;">
            锁定页面展示宽度：<input type="range" name="iframeWidthSet" min="0" max="1920" id="iframeWidthSet" step="1" value="0" onchange="iframeWidthSet()"/>
        </p>
    <p  class="iframeHeightSetP">
            锁定页面展示高度：<input type="range" name="points" min="0" max="10000" id="iframeHeightSet" step="1" value="0" onchange="setIframeHeight()"/>
    </p>
    <div class="serachP">
        <p class="addDir">
            <span onclick="openEdit()" id="openEdit">打开编辑器</span> | 
            <input id="addDirName" type="text"/>
            <input name="addDir" type="radio" value="file" checked="checked"/> 文件
            <input name="addDir" type="radio" value="dir"  /> 目录      
            <button class="addDirbutton" onclick="addDir()">新建</button> 
        </p>
        当前路径：<span class="nowPath"></span>
        <ul class="dirLists"></ul>
    </div> 
    <span class="close" onclick="closeSetHetBox(this)"></span> 
</div>
<div class="box">
        选择模式：
        <select name="mode" id="debugModel" class="debugModel"  onchange="changeModel()">
            <option value="NORMAL">普通网页</option>
            <option value="GET">Api.get</option>
            <option value="POST">Api.post</option>
            <option value="YALI-GET">压力测试.网页</option>
            <option value="YALI-POST">压测Api.post</option>
            <option value="REGXP">正则表达式</option>
            <option value="CHEST">百宝箱</option>
        </select>
        <select name="mode2" id="debugModel2" class="debugModel2" >
            <option value="CUSTOM">手动模式</option>
            <option value="AUTO">自动模式</option>
        </select>
        <input type="hidden" id="isOpen" value="no">
        <input type="text" id="url" placeholder="大神，是时候填上你要debug的操作了！" onblur="updateModelsUrlValues()">
        <span class="Interval YALISHOW">间隔：<input type="text" id="Second" value="5"> 秒</span>
        <button id="DeBug" onclick="DeBug()">开始</button>
        <span class='YALISHOW'><button id="stop" onclick="stop()" style="background: #000;">停止</button></span>
        <button id="OpenMysql" onclick="OpenMysql()" data-status="yes">打开mysql监控</button>
        <button id="hiden" onclick="hidenDebug()">最小化</button>
        <div class="params">
            <div class="bingfa-params">
                并发数：<input type="text" class="C" value="10">
                链接数：<input type="text" class="N" value="1000">
                是否自动继续压测：<input type="checkbox" class="isAutoYaCe" value="yes">
                <br/>
                <br/>
                <div class="AgreementBox">
                    <input type="checkbox" name="agreement" class="agreement" value="ok"> 
                    同意与注意：
                </div>
                <p>1:请遵守互联网法律法规。</p>
                <p>2:并发数不能大于链接数。</p>
                <p>3:Mysql压测期间后台会关闭监控。</p>
                <p>4:压力测试时请不要开启多个调试页面。</p>
                <p>5:并发数与链接数最好成整数倍，最好10倍以上比较好。</p>
                <p>6:请debug自己的站点或者api，请勿恶意去测试别人站点。</p>
                <p>7:本工具仅供学习调试使用，请勿使用该功能用于非法用途，如果触犯相关法律概不负责，且发现将收回使用权限。</p>
                <p>8:你勾选同意将视为同意以上条款，使用此功能时您的IP，压测记录将记录到远程，本工具已多次提醒和建议只能用于学习调试，不得用于非法目的，如果你的行为触犯相关法律，与本作者无关，本作者将配合警方提供你的犯罪记录，概不负责。</p>
            </div>

            <button  class="addParam"  onclick="ClearSql()" style="background:red;">清空SQL</button>
            <!-- <button  class="addParam"  onclick="GetSql()">提取当前SQL</button> -->
            <button  class="addParam"  onclick="GetAllDataBases()" style="background:green;">SQL执行分析</button>
            <button  class="addParam"  onclick="openPerformance()" style="background: purple;">开启性能报表</button>
            <button  class="addParam"  onclick="jsonFormat()" style="background: slateblue;">json格式化</button>
            <button  class="addParam"  onclick="openQL()" style="background:blue;">打开学习交流区</button>
            <button  class="addParam"  onclick="addParam()" style="background:blueviolet;">添加参数</button>
            api响应语法：
            <select name="mode" id="ApiResponsModel" class="ApiResponsModel"  onchange="setApiResponsModel()">
                <option value="json">json</option>
                <option value="html">html</option>
                <option value="xml">xml</option>
                <option value="text">文本</option>
            </select>
            <span class="lockPageDisplaySpan"> 设置与代码热编译：<input type="checkbox" name="lockPageDisplay" id="lockPageDisplay"    onclick="lockPageDisplay()"></span>
            <em id="ShowMsg"></em>
            <div class="params-list">
                <form action="#" id="params-form">
                </form>
            </div>
        </div>
        <div class="regText">
                <p>
                    全局搜索：<input type="checkbox" name="regGlobal" class="regGlobal"  id="regGlobal" value="ok" checked>  &nbsp;&nbsp;&nbsp;&nbsp;
                    区分大小写：<input type="checkbox" name="regCase" class="regCase"  id="regCase" value="ok" checked>  &nbsp;&nbsp;&nbsp;&nbsp;
                    页面抓取：<input type="checkbox" name="pageGet" class="pageGet"  id="pageGet"  onclick="pageGet()">  &nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="pageGetBox">
                        网址：<input type="text" id="pageGetInput" placeholder="请遵守robots.txt协议和互联网法律法规哦！">
                        <button onclick="GetPageContent()" id="GetPageContent">走你</button>
                    </span>
                    <span class='replaceSpan'></span>
                </p>
                <p>内容：<textarea name="regStr" id="regStr" class="regStr" onkeyup="regXp()" onblur="regXp()"></textarea></p>
       </div>
       <div class="chestBox">
           <p>
                <span class="params-span " onclick="setChestParamsChecked(this)"><input name="chest" type="radio" value="STRSEARCH" onclick="changeChest(0)"  /> 文件系统管理(字符串搜索替换)</span>
                <span class="params-span " onclick="setChestParamsChecked(this)" ><input name="chest" type="radio" value="DIRSEARCH" onclick="changeChest(0)"  /> 目录或文件搜索</span>
                <span class="params-span " onclick="setChestParamsChecked(this)" ><input name="chest" type="radio" value="IPTOCITY" onclick="changeChest(0)"/> IP查询  </span>
                <span class="params-span " onclick="setChestParamsChecked(this)" ><input name="chest" type="radio" value="TIMETODATE" onclick="changeChest(0)"/> 时间戳转日期 </span>
           </p>
           <div class="serachP">
               当前路径：<span class="nowPath"></span>
               <ul class="dirLists"></ul>
           </div>  
       </div>
</div>
<div id="Mysqlbox">

</div>
<div class="codeBox">
    <div class="codeInstructions">
        <p  class="codeBoxWidthSetP">
            修改页面宽度：
            <input type="range" class="codeBoxWidthRange" min="0" max="1920" id="codeBoxWidthRange" step="1" value="960" onchange="codeBoxWidthSet()">
        </p>
        <p>代码编辑时按住ctrl+s会自动保存，如果编辑的文件在网页，api.get，api.post三个模式下的目录里面会实时编译自动刷新</p>
        <p style="display: none;">当前打开文件路径:&nbsp;<span class="fileDir"></span></p>
        <p>字体大小:&nbsp;
            <input type="text" onkeyup="SetFontSize(this)" value="16" class="SetFontSize">
            编程语言/框架修改:
            <select name="SetLang" id="SetLang" onchange="SetLang()"></select>
            历史版本:&nbsp;
            <select name="editHistoryList" id="editHistoryList"  onchange="changeBanben(this)"></select>
        </p>
        <p><span class="fileLists"></span></p>
    </div>
    <!-- <div class="codeMain" id="codeMain" onkeyup="Code()"> -->
    <div class="codeMain" id="codeMain" onkeyup="editCode()" >

    </div>
    <span class="close" onclick="closeCodeBox(this)"></span>
</div>
<div href="javascript:;" class="openDebug" onclick="openDebug()">打开调试</div>
<div style="display:none;" class="editorBox"><div id="editor"></div></div>
<div class="QL">
    <p class="QL-title">技术学习交流区</p>
    <div class="ContactList"></div>
    <dl class="">
    </dl>
    <div class="UserLists"></div>
   <p class="sendBox">
        <button class="expression" onclick="expressionInfo()">表情</button> 
       <input type="text" id="msg" placeholder="请输入您感兴趣的技术话题" /> <button onclick="sendMsg()">发送</button>  
 </p>
 <div class="expressionInfo">
        <p>
            网络图片:<input type="text" id="netImgUrl" placeholder="请输入您喜欢的网络表情图片,发送后表情会收藏" />
            <button onclick="sendNetImg()" class="sendExpression">发送</button> 
         </p>
            <p class="expressionList"></p>
        <span class="close" onclick="closeexpressionInfo()"></span>
 </div>

 <span class="close" onclick="closeQL(this)"></span>
 <span class="msgSound"></span>
</div>
</body>
</html>